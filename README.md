# Packer-OpenSuse.13.2

This [Packer][1] template is setting up a very basic OpenSuSE 13.2 server installation for use with vagrant 

_*Currently the only working packer profile in this template is opensuse-ami*_

The AMI is based on the official OpenSUSE 13.2 HVM AMI as listed in a [forum post][1]

The [VMWare workstation][3] and [VirtualBox][2] providers might work, but proably not.

The only user is root.

The only way to login is by using the keys that are also set up as part of the template.

## How To

I ran this on an OpenSuSE 13.2 desktop.


1. set the your AWS access key and secret as an environment variable
    
    export AWS_ACCESS_KEY_ID=AKIASOMETHINGOROTHER
    export AWS_SECRET_ACCESS_KEY=n/sometHingOrOtherSomeHinG

2. run
    
    :::Shell
    packer build -only=opensuse-ami packer-opensuse-13.2-baseinstall.json
       

_NB_
The "amazon-instance" build will not work for the used OpenSUSE AMI. The OpenSUSE AMI is a hvm ebs based AMI with a GP2 EBS volume.
All other OpenSUSE in ap-souteast-2 listed at [3] are not able to run  on a T2.small  because they are paravirtual.
When building the volume with ec2-bundle-vol it fails with "non-standard volume" (the volume is a GP2 volume and not a standard volume)


       
## Todo :

* Fix the other builds


[1]: http://www.packer.io        "Packer"
[2]: http://lists.opensuse.org/opensuse/2014-11/msg00115.html "OpenSuSE 13.2 AMI annoucement" 
[3]: http://www.virtualbox.org   "VirtualBox"
[4]: http://www.vmware.com/au/products/workstation  "VMWare Workstation"

