#/bin/bash

# change the way the network starts since there seems to be a duplicate start up
# when the network is auto udev or something else seems to start the network on eht0
# then systemd or something else seems to do it again and shortly disconnect the network 
# again.

sed -i "s~STARTMODE='auto'~STARTMODE='ifplugd'~" /etc/sysconfig/network/ifcfg-eth0

