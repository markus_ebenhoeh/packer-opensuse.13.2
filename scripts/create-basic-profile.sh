cat > /etc/profile.local <<`EOF`

function fnd()
{
   if [[ $# -lt 1 ]];then
      return 1
   fi
   if [[ $# -lt 2 ]];then
      dirs="."
      suchstring="$1"
   else
      dirs="$1"
      suchstring="$2"
   fi
   #-o .* for debian
   find $dirs \( -iname "*$suchstring*" -o -iname ".*$suchstring*" \)  $3 $4 $5 $6 $7 $8 $9 ${10} ${11} ${12} -printf "%11s %u:%g %TY%Tm%Td_%TH%TM%TS %p\n"
}
function findgrep()
{
   if [[ $# -lt 2 ]];then
      dirs="."
      suchstring="$1"
   else
      dirs="$1"
      suchstring="$2"
   fi
   echo find $4 $dirs -type f $3 -print0 "|" xargs --null --max-args 2000 grep -i $4 "$2"
   find $4 $dirs -type f $3 -print0 | xargs --null --max-args 2000 grep -i $4 "$suchstring"
}
function findls()
{
   if [[ $# -lt 1 ]];then
      find . -type f -printf "%11s  %TY%Tm%Td_%TH%TM%TS  %p\n"
   else
      find $1 -type f -printf "%11s  %TY%Tm%Td_%TH%TM%TS  %p\n" $2 $3 $4 $5 $6 $7 $8 $9
   fi
}
function dateecho()
(
   echo -e $(date "+%Y-%m-%d %H:%M:%S.%N")\\t$*
)

function mdates()
(
   date "+%Y%m%d_%H%M%S"
)
function mdatem()
(
   date "+%Y%m%d_%H%M"
)
function mdate()
(
   date "+%Y%m%d"
)

function mess() 
{
   journalctl --lines=1000  -f
}
function wwwerrlog()
{
   tail --lines=1000 -F /var/log/apache2/error_log
}
function acclog()
{
   tail --lines=1000 -F /var/log/apache2/access_log
}

function tf()
{
   if [[ $# -lt 1 ]];then
      echo "use 'tf filename' filename will then be tailed with tail --lines 1000 --follow"
   else
   filename=${1##*/}
      tail --lines=1000 -F $1
   fi
}





alias lr='ls -lisart --time-style "+%Y-%m-%d %H:%M:%S"'
alias more=less
alias hex='od -Ax -tx1 -w16  -tc'

# cd aliases

alias cdlog='cd /var/log'
alias cdht='cd /srv/www/htdocs'

alias psg='ps -ef | grep -v grep | grep -i'
alias psme='ps -eo pid,ppid,user,%cpu,%mem,sz,rss,vsize,cmd'
alias psme2='ps -eo pid,ppid,user,%cpu,%mem,sz,rss,vsize,cmd | sort -k 9 '
alias lm='lsmod | grep -i '
alias rpg='rpm -q -a | grep -i'
alias ng='netstat -apn | grep -i'
alias setg='set | grep -i'

# search (shell) history
alias gist='history | cut -c 8- | grep -i'


HISTFILESIZE=2000
HISTSIZE=2000

`EOF`


