#!/bin/bash

if [[ ! -f README.md ]]; then
   echo $0 has to be run from the root folder of the project
   exit -21
fi

mkdir -p key

rm -f key/id_rsa_packer_images.private
ssh-keygen -q -C "packer image creation ssh key" -N "" -f key/id_rsa_packer_images.private
mv key/id_rsa_packer_images.private.pub key/id_rsa_packer_images.pub

cp key/id_rsa_packer_images.pub http/
