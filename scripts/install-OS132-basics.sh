#!/bin/bash

zypper --non-interactive install unzip rsync ruby less php apache2 mysql

zypper --non-interactive refresh
zypper --non-interactive update
