#!/bin/bash

t=/tmp/update-script

# on VirtualBox SuSE seems to forget the way that I refer to devices.
# this reverts it back to a working value that is indpenedent of 
# the device id
echo "(hd0)   /dev/sda" > /boot/grub2/device.map

cat >${t} <<ENDL

date

zypper --non-interactive modifyrepo --disable --no-refresh openSUSE-12.3-1.7
zypper --non-interactive addrepo --check --refresh --name "Update Repository (Non-Oss)" http://download.opensuse.org/update/12.3-non-oss/ download.opensuse.org-12.3-non-oss
zypper --non-interactive addrepo --check --refresh --name "Main Repository (NON-OSS)" http://download.opensuse.org/distribution/12.3/repo/non-oss/ download.opensuse.org-non-oss   
zypper --non-interactive addrepo --check --refresh --name "Main Repository (OSS)" http://download.opensuse.org/distribution/12.3/repo/oss/ download.opensuse.org-oss     
zypper --non-interactive addrepo --check --refresh --name "Main Update Repository" http://download.opensuse.org/update/12.3/ download.opensuse.org-update 
zypper --non-interactive refresh
zypper --non-interactive update

date

sleep 10 

reboot

# to make sure we wait for the reboot 
sleep 10 

ENDL

chmod u+x ${t}

echo storing output under VM : ${t}.out
nohup bash -vx ${t} > ${t}.out &

echo Initiating sshd shutdown and killing all ssh* processes.
echo The install process will seems to hang for serveral minutes 
echo while the updates are installed.
echo the reboot at the end will start sshd and the process will continue

# prevent reconnect for next script until after the time out
systemctl  stop sshd
ps -ef | grep ssh | grep -v grep  | awk '{print $2}' | xargs kill

